import 'package:raffle_up/models/app_state.dart';
import 'package:raffle_up/reducers/raffle_items_reducer.dart';

AppState appReducer(AppState state, action) {
  return AppState(raffleItems: raffleItemsReducer(state.raffleItems, action));
}
