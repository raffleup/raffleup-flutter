import 'package:raffle_up/actions/raffle_item_actions.dart';
import 'package:raffle_up/models/raffle_item.dart';
import 'package:redux/redux.dart';

final raffleItemsReducer = combineReducers<List<RaffleItem>>(
    [TypedReducer<List<RaffleItem>, AddRaffleItemAction>(_addRaffleItem)]);

List<RaffleItem> _addRaffleItem(
    List<RaffleItem> items, AddRaffleItemAction action) {
  return List.from(items)..add(action.newRaffleItem);
}
