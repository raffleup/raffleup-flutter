import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:raffle_up/models/app_state.dart';
import 'package:raffle_up/models/raffle_item.dart';
import 'package:raffle_up/presentation/featured/carousel_item.dart';
import 'package:raffle_up/presentation/featured/featured.dart';
import 'package:raffle_up/presentation/featured/raffle_item_preview.dart';
import 'package:redux/redux.dart';

class FeaturedPageContainer extends StatelessWidget {
  Widget build(BuildContext buildContext) {
    return new StoreConnector<AppState, _ViewModel>(
        converter: _ViewModel.fromStore,
        builder: (BuildContext context, _ViewModel vm) {
          return new FeaturedPage(
              vm.featuredItems, vm.popularItems, vm.newItems);
        });
  }
}

class _ViewModel {
  final List<Widget> featuredItems;
  final List<Widget> popularItems;
  final List<Widget> newItems;

  _ViewModel(this.featuredItems, this.popularItems, this.newItems);

  //Generate featured, popular, and new lists here.
  static _ViewModel fromStore(Store<AppState> store) {
    List<RaffleItem> raffleItems = store.state.raffleItems;

    List<RaffleItem> popularItems = raffleItems.sublist(0, 9);
    List<RaffleItem> newItems = raffleItems.sublist(10);

    List<Widget> popularItemWidgets = new List<Widget>();
    for (var i = 0; i < popularItems.length; i++) {
      RaffleItem item = popularItems[i];
      popularItemWidgets.add(new RaffleItemPreview(
          item.name,
          item.cost.toString(),
          item.quantity.toString(),
          item.maxQuantity.toString(),
          null));
    }

    List<Widget> newItemWidgets = new List<Widget>();
    for (var i = 0; i < newItems.length; i++) {
      RaffleItem item = newItems[i];
      newItemWidgets.add(new RaffleItemPreview(item.name, item.cost.toString(),
          item.quantity.toString(), item.maxQuantity.toString(), null));
    }

    List<Widget> featuredItemWidgets = new List<Widget>();
    for (var i = 0; i < 10; i++) {
      featuredItemWidgets.add(new CarouselItem());
    }

    return new _ViewModel(
        featuredItemWidgets, popularItemWidgets, newItemWidgets);
  }
}
