import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:raffle_up/models/app_state.dart';
import 'package:raffle_up/models/user.dart';
import 'package:raffle_up/presentation/account/account.dart';
import 'package:raffle_up/presentation/account/user_preview.dart';
import 'package:redux/redux.dart';

class AccountPageContainer extends StatelessWidget {
  Widget build(BuildContext buildContext) {
    return new StoreConnector<AppState, _ViewModel>(
        converter: _ViewModel.fromStore,
        builder: (BuildContext context, _ViewModel vm) {
          return new AccountPage(vm.user);
        });
  }
}

class _ViewModel {
  final List<Widget> user;

  _ViewModel(this.user);

  //Generate category lists here.
  static _ViewModel fromStore(Store<AppState> store) {
    User user = store.state.user;

    List<Widget> list1 = new List<Widget>();

    list1.add(new UserPreview(user.name, user.imageURL));

    return new _ViewModel(list1);
  }
}
