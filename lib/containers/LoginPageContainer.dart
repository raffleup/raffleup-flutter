import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:raffle_up/models/app_state.dart';
import 'package:raffle_up/presentation/login/login.dart';
import 'package:redux/redux.dart';

class LoginPageContainer extends StatelessWidget {
  Widget build(BuildContext buildContext) {
    return new StoreConnector<AppState, _ViewModel>(
        converter: _ViewModel.fromStore,
        builder: (BuildContext context, _ViewModel vm) {
          return new LoginPage();
        });
  }
}

class _ViewModel {
  static _ViewModel fromStore(Store<AppState> store) {
    return new _ViewModel();
  }
}
