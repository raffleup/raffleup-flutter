import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:raffle_up/models/app_state.dart';
import 'package:raffle_up/models/raffle_item.dart';
import 'package:raffle_up/presentation/payment/payment.dart';
import 'package:redux/redux.dart';

class PaymentPageContainer extends StatelessWidget {
  Widget build(BuildContext buildContext) {
    return new StoreConnector<AppState, _ViewModel>(
        converter: _ViewModel.fromStore,
        builder: (BuildContext context, _ViewModel vm) {
          return new PaymentPage(
              vm.itemImageURL, vm.itemName, vm.itemDescription);
        });
  }
}

class _ViewModel {
  final String itemImageURL;
  final String itemName;
  final String itemDescription;

  _ViewModel(this.itemImageURL, this.itemName, this.itemDescription);

  //Generate featured, popular, and new lists here.
  static _ViewModel fromStore(Store<AppState> store) {
    List<RaffleItem> raffleItems = store.state.raffleItems;

    RaffleItem raffleItem = raffleItems[new Random().nextInt(9)];

    String name = "Yeezy Boost 350 V2 - 'Cream'";
    String description = 'Sizing available: 8, 9, 10, 11\n\nThe adidas Yeezy Boost 350 v2 is a low-top sneaker designed by Kanye West and adidas. The shoe is famous for the “SPLY-350” patterns on the upper. The latest “Butter” colorway—a soft yellow—is scheduled to release June 30th and does not feature SPLY-350 branding, while the readily available “Cream” colorway is slated to restock the following month.';
    return new _ViewModel(
      "assets/shoes/1.jpg", name, description);
  }
}
