import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:raffle_up/models/app_state.dart';
import 'package:raffle_up/models/category_item.dart';
import 'package:raffle_up/presentation/category/category.dart';
import 'package:raffle_up/presentation/category/category_preview.dart';
import 'package:redux/redux.dart';

class CategoryPageContainer extends StatelessWidget {
  Widget build(BuildContext buildContext) {
    return new StoreConnector<AppState, _ViewModel>(
        converter: _ViewModel.fromStore,
        builder: (BuildContext context, _ViewModel vm) {
          return new CategoryPage(vm.categories);
        });
  }
}

class _ViewModel {
  final List<Widget> categories;

  _ViewModel(this.categories);

  //Generate featured, popular, and new lists here.
  static _ViewModel fromStore(Store<AppState> store) {
    List<ItemCategory> categories = store.state.categories;

    List<Widget> list1 = new List<Widget>();

    for (var category in categories) {
      list1.add(new CategoryPreview(category.name, category.imageURL));
    }

    return new _ViewModel(list1);

    //return new _ViewModel(store.state.raffleItems, store.state.raffleItems, store.state.raffleItems);
  }
}
