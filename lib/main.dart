import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:raffle_up/models/app_state.dart';
import 'package:raffle_up/routes.dart';
import 'package:raffle_up/presentation/template_page.dart';
import 'package:raffle_up/presentation/base_page.dart';
import 'package:raffle_up/reducers/app_state_reducer.dart';
import 'package:redux/redux.dart';

void main() => runApp(new App());

class App extends StatelessWidget {
  final Store<AppState> store = Store<AppState>(
    appReducer,
    initialState: AppState.initial(),
    //middleware: createMiddleware()
  );

  @override
  Widget build(BuildContext context) {
    return new StoreProvider(
        store: this.store,
        child: new MaterialApp(
            title: 'Clover',
            routes: routes,
            //Base Page doesn't need any non-self-contained state,
            //so we can just instantiate it directly here.
            home: new BasePage()));
  }
}
