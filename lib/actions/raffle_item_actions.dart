import 'package:raffle_up/models/raffle_item.dart';

class AddRaffleItemAction {
  final RaffleItem newRaffleItem;

  AddRaffleItemAction(this.newRaffleItem);
}

class RefreshRaffleItemsAction {}
