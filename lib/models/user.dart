class User {
  final String name;
  final String imageURL;

  User(this.name, this.imageURL);
}
