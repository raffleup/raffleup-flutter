import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:raffle_up/models/category_item.dart';
import 'package:raffle_up/models/raffle_item.dart';
import 'package:raffle_up/models/user.dart';

@immutable
class AppState {
  final List<RaffleItem> raffleItems;
  final List<ItemCategory> categories;
  final Object user;

  AppState(
      {this.raffleItems = const [], this.categories = const [], this.user});

  factory AppState.initial() {
    // Generate some fake items for now
    List<RaffleItem> items = [];
    for (var i = 0; i < 20; i++) {
      items.add(new RaffleItem(
          "Item " + i.toString(),
          new Random().nextInt(500),
          "Description for item " + i.toString(),
          "Seller " + new Random().nextInt(15).toString(),
          i * 10,
          i * 10 + new Random().nextInt(100)));
    }

    return AppState(
      raffleItems: items,
      categories: [
        new ItemCategory("Shoes", "assets/categories/shoes.jpg"),
        new ItemCategory("Clothes", "assets/categories/clothes.jpg"),
        new ItemCategory("Electronics", "assets/categories/electronics.jpg"),
        new ItemCategory("Bags", "assets/categories/bags.jpg"),
        new ItemCategory("Accessories", "assets/categories/accessories.jpg"),
        new ItemCategory("Misc.", "assets/categories/misc.jpg"),
      ],
      user: new User("Alec Rosenbach",
          "https://raw.githubusercontent.com/flutter/website/master/_includes/code/layout/lakes/images/lake.jpg"),
    );
  }

  AppState copyWith({List<RaffleItem> raffleItems}) {
    return new AppState(
      raffleItems: raffleItems ?? this.raffleItems,
    );
  }
}
