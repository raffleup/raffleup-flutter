class RaffleItem {
  final String name;
  final int cost;
  final String description;
  final String seller;
  final int quantity;
  final int maxQuantity;

  RaffleItem(this.name, this.cost, this.description, this.seller, this.quantity,
      this.maxQuantity);
}
