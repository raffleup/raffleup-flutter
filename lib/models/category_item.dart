class ItemCategory {
  final String name;
  final String imageURL;

  ItemCategory(this.name, this.imageURL);
}
