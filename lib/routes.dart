import 'package:flutter/material.dart';
import 'package:raffle_up/presentation/template_page.dart';
import 'package:raffle_up/presentation/base_page.dart';

final routes = {
  '/login':         (BuildContext context) => new BasePage(),
  '/home':         (BuildContext context) => new TemplatePage(),
};