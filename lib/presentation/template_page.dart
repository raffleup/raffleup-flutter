import 'package:flutter/material.dart';
import 'package:raffle_up/containers/AccountPageContainer.dart';
import 'package:raffle_up/containers/CategoryPageContainer.dart';
import 'package:raffle_up/containers/FeaturedPageContainer.dart';

class TemplatePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _TemplatePageState();
  }
}

class _TemplatePageState extends State<TemplatePage> {
  PageController _pageController;
  int _currentPage = 0;

  @override
  void initState() {
    super.initState();
    _pageController = new PageController();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: true,
      body: new Scaffold(
          appBar: new AppBar(
              title: const Text('Clover'),
              backgroundColor: Colors.green,
              actions: <Widget>[
                new IconButton(
                  icon: new Icon(Icons.search),
                  onPressed: () {
                    //TODO : Show search bar
                  },
                )
              ]),
            body: new PageView(
            children: <Widget>[
              new FeaturedPageContainer(),
              new CategoryPageContainer(),
              new AccountPageContainer()
            ],
            controller: _pageController,
            onPageChanged: (int page) {
              setState(() {
                this._currentPage = page;
              });
            },
          )),
      bottomNavigationBar: new BottomNavigationBar(
        items: [
          new BottomNavigationBarItem(
              icon: new Icon(Icons.star), title: new Text("Featured")),
          new BottomNavigationBarItem(
              icon: new Icon(Icons.shopping_basket),
              title: new Text("Categories")),
          new BottomNavigationBarItem(
              icon: new Icon(Icons.people), title: new Text("Account")),
        ],
        onTap: (int page) {
          _pageController.animateToPage(page,
              duration: const Duration(milliseconds: 300), curve: Curves.ease);
        },
        currentIndex: _currentPage,
      ),
    );
  }
}
