import 'package:flutter/material.dart';
import 'package:raffle_up/containers/PaymentPageContainer.dart';

///Item Preview
///Represents a single item preview on the homepage.
///Contains an image thumbnail, pricing information, ticket sales,
///and a button to view more information.
class CategoryPreview extends StatelessWidget {
  final String name;
  final String imageURL;

  CategoryPreview(this.name, this.imageURL);

  @override
  Widget build(BuildContext build) {
    Container container = new Container(
        padding: new EdgeInsets.all(8.0),
        width: 50.0,
        child: new SizedBox(
            width: 20.0,
            height: 20.0,
            child: new RaisedButton(
                padding: EdgeInsets.zero,
                color: Colors.red, //TODO : remove
                onPressed: () {
                  Scaffold.of(build).showSnackBar(new SnackBar(
                        content: new Text("TODO : Open individual category"),
                      ));
                  Navigator.push(
                    build, 
                    MaterialPageRoute(
                      builder: (build) => new PaymentPageContainer()
                    )
                  );
                  /// TODO : Direct to specific category page
                },
                child: new Container(
                    padding: EdgeInsets.zero,
                    alignment: Alignment.center,
                    color: Colors.blue, //TODO : remove
                    child: new Stack(
                      alignment: Alignment.center,
                      //fit: StackFit.passthrough,
                      children: <Widget>[
                        new Image.asset(this.imageURL),
                        new Container(
                          decoration: new BoxDecoration
                          (
                            color: Colors.green
                          ),
                          child: new Text(
                            this.name,
                            style: new TextStyle(
                              color: Colors.white,
                              fontSize: 24.0,
                              fontWeight: FontWeight.bold
                            ),
                          ),
                        )
                      ],
                    )))));
    return container;
  }
}
