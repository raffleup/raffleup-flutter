import 'package:flutter/material.dart';

///Categories Page

class CategoryPage extends StatelessWidget {
  final List<Widget> categories;

  CategoryPage(this.categories);

  @override
  Widget build(BuildContext build) {
    Container container = new Container(
        color: Colors.white,
        child: new Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Expanded(
                  child: new GridView.count(
                crossAxisCount: 2,
                children: this.categories,
              ))
            ]));
    return container;
  }
}