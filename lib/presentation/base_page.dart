import 'package:flutter/material.dart';
import 'package:raffle_up/containers/LoginPageContainer.dart';
// import 'package:raffle_up/containers/RegisterPageContainer.dart';

class BasePage extends StatefulWidget {
  @override
  State createState() => new _BasePageState();
}

class _BasePageState extends State<BasePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomPadding: false,
        body: new PageView(
          children: <Widget>[
            new LoginPageContainer(),
          ]
        )
    );
  }
}