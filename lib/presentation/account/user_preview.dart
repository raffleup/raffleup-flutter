import 'package:flutter/material.dart';

class UserPreview extends StatelessWidget {
  final String name;
  final String imageURL;

  UserPreview(this.name, this.imageURL);

  @override
  Widget build(BuildContext build) {
    Container container = new Container(
      margin: new EdgeInsets.only(top: 5.0),
      child: new Column(children: [
        new CircleAvatar(
          backgroundImage: new NetworkImage(imageURL),
          radius: 75.0,
        ),
        new Text(
          name,
          textAlign: TextAlign.center,
          style: new TextStyle(fontSize: 28.0, color: Colors.white),
        ),
      ]),
    );
    return container;
  }
}
