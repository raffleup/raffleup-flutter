import 'package:flutter/material.dart';
import 'package:raffle_up/presentation/account/account_button.dart';
import 'package:raffle_up/presentation/account/logout_button.dart';

/*
Account Page
*/
class AccountPage extends StatelessWidget {
  final List<Widget> user;

  AccountPage(this.user);

  @override
  Widget build(BuildContext build) {
    Container container = new Container(
      color: Colors.green[800],
      width: MediaQuery.of(build).size.width,
      child: new ListView(
        scrollDirection: Axis.vertical,
        children: [
          new Padding(
            padding: new EdgeInsets.symmetric(vertical: 10.0),
            child: new Column(children: this.user),
          ),
          new Column(children: [
            new AccountButton("Current Entries", null),
            new AccountButton("Purchase History", null),
            new AccountButton("Payment Methods", null),
            new AccountButton("Account Details", null),
            new LogoutButton(null),
          ]),
        ],
      ),
    );
    return container;
  }
}
