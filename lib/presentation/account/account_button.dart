import 'package:flutter/material.dart';

class AccountButton extends StatelessWidget {
  final GestureTapCallback tapCallback;
  final String buttonText;

  AccountButton(this.buttonText, this.tapCallback);

  @override
  Widget build(BuildContext build) {
    Container container = new Container(
      margin: new EdgeInsets.only(top: 5.0),
      child: new InkWell(
        onTap: () {
          Scaffold.of(build).showSnackBar(new SnackBar(
                content: new Text("TODO : Open individual category"),
              ));

          /// TODO : Direct to specific category page
        },
        child: new FractionallySizedBox(
          alignment: FractionalOffset.center,
          widthFactor: 0.75,
          child: new Container(
            height: 50.0,
            decoration: new BoxDecoration(
              color: Colors.black26,
              borderRadius: new BorderRadius.circular(10.0),
              border: new Border.all(color: Colors.white, width: 1.0),
            ),
            child: new Center(
              child: new Text(
                buttonText,
                style: new TextStyle(fontSize: 20.0, color: Colors.white),
              ),
            ),
          ),
        ),
      ),
    );
    return container;
  }
}
