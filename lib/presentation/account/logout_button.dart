import 'package:flutter/material.dart';

class LogoutButton extends StatelessWidget {
  final GestureTapCallback tapCallback;

  LogoutButton(this.tapCallback);

  @override
  Widget build(BuildContext build) {
    Container container = new Container(
      margin: new EdgeInsets.only(top: 10.0),
      child: new InkWell(
        onTap: () {
          Navigator.pop(build);
        },
        child: new FractionallySizedBox(
          alignment: FractionalOffset.center,
          widthFactor: 0.25,
          child: new Container(
            height: 37.5,
            decoration: new BoxDecoration(
              color: Colors.red,
              borderRadius: new BorderRadius.circular(10.0),
              border: new Border.all(color: Colors.white, width: 1.0),
            ),
            child: new Center(
              child: new Text(
                "Logout",
                style: new TextStyle(fontSize: 18.0, color: Colors.white),
              ),
            ),
          ),
        ),
      ),
    );
    return container;
  }
}
