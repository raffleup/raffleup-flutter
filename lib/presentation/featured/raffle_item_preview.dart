import 'dart:math';

import 'package:flutter/material.dart';

///Item Preview
///Represents a single item preview on the homepage.
///Contains an image thumbnail, pricing information, ticket sales,
///and a button to view more information.
class RaffleItemPreview extends StatelessWidget {
  final String name;
  final String cost;
  final String quantity;
  final String maxQuantity;
  final GestureTapCallback tapCallback;

  RaffleItemPreview(
      this.name, this.cost, this.quantity, this.maxQuantity, this.tapCallback);

  @override
  Widget build(BuildContext build) {
    Container container = new Container(
        padding: new EdgeInsets.only(left: 5.0, right: 10.0),
        child: new SizedBox(
            width: 150.0,
            height: 150.0,
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Image.asset("assets/shoes/" +
                    (1 + new Random().nextInt(4)).toString() +
                    ".jpg"),
                new Container(
                    padding: new EdgeInsets.only(left: 15.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          name,
                          style: new TextStyle(
                              fontSize: 17.0, fontWeight: FontWeight.w600),
                        ),
                        new Text(
                          " \$" + cost + "/ticket",
                          style: new TextStyle(
                              //fontSize: 17.0,
                              fontWeight: FontWeight.w200),
                        ),
                        new Text(
                          " " + quantity + "/" + maxQuantity + " sold",
                          style: new TextStyle(
                              //fontSize: 17.0,
                              fontWeight: FontWeight.w200),
                        )
                      ],
                    )),
                new FractionallySizedBox(
                    widthFactor: 0.8,
                    child: new RaisedButton(
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                      onPressed: tapCallback,
                      child: const Text("Enter"),
                      color: Colors.green,
                      textColor: Colors.white,
                    ))
              ],
            )));
    return container;
  }
}
