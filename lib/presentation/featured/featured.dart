import 'package:flutter/material.dart';

///Featured Page

class FeaturedPage extends StatelessWidget {
  final List<Widget> featuredItems;
  final List<Widget> popularItems;
  final List<Widget> newItems;

  FeaturedPage(this.featuredItems, this.popularItems, this.newItems);

  @override
  Widget build(BuildContext build) {
    Container container = new Container(
        color: Colors.white,
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // Text starts from left instead of center
          children: <Widget>[
            new SizedBox(
                height: MediaQuery.of(build).size.width * 0.70,
                child: new PageView(children: this.featuredItems)),
            new SizedBox(
                width: 500.0,
                height: 252.0,
                child: new ListView(
                    padding: new EdgeInsets.only(top: 10.0),
                    scrollDirection: Axis.vertical,
                    children: <Widget>[
                      new Container(
                          padding: new EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 3.0),
                          child: new TextField(
                              enabled: false,
                              decoration: new InputDecoration(
                                  contentPadding:
                                      new EdgeInsets.only(bottom: 5.0),
                                  border: new UnderlineInputBorder(),
                                  hintText: "Popular",
                                  hintStyle:
                                      new TextStyle(color: Colors.black)),
                              style: new TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold,
                              ))),
                      new SizedBox(
                          width: 500.0,
                          height: 220.0,
                          child: new ListView(
                              padding: new EdgeInsets.all(8.0),
                              scrollDirection: Axis.horizontal,
                              children: this.popularItems)),
                      new Container(
                          padding: new EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 3.0),
                          child: new TextField(
                              enabled: false,
                              decoration: new InputDecoration(
                                  contentPadding:
                                      new EdgeInsets.only(bottom: 5.0),
                                  border: new UnderlineInputBorder(),
                                  hintText: "What's new",
                                  hintStyle:
                                      new TextStyle(color: Colors.black)),
                              style: new TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold,
                              ))),
                      new SizedBox(
                          width: 500.0,
                          height: 220.0,
                          child: new ListView(
                              padding: new EdgeInsets.all(8.0),
                              scrollDirection: Axis.horizontal,
                              children: this.newItems)),
                    ])),
          ],
        ));
    return container;
  }
}
