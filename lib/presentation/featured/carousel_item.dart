import 'dart:math';

import 'package:flutter/cupertino.dart';

class CarouselItem extends StatelessWidget {
  CarouselItem();

  @override
  Widget build(BuildContext build) {
    return new Container(
        child: new SizedBox(
            width: 150.0,
            height: 100.0,
            child: Image.asset("assets/shoes/" +
                (1 + new Random().nextInt(4)).toString() +
                ".jpg")));
  }
}
