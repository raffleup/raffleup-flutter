import 'package:flutter/material.dart';

///Featured Page

class PaymentPage extends StatelessWidget {
  final String itemImageURL;
  final String itemName;
  final String itemDescription;

  PaymentPage(this.itemImageURL, this.itemName, this.itemDescription);

  @override
  Widget build(BuildContext build) {
    Container container = new Container(
      color: Colors.white,
      child: new Stack(
        children: [
          new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            // Text starts from left instead of center
            children: <Widget>[
              new SizedBox(
                height: MediaQuery.of(build).size.width * 0.70,
                child: new Image.asset(this.itemImageURL),
              ),
              new SizedBox(
                width: 500.0,
                height: 252.0,
                child: new ListView(
                  padding: new EdgeInsets.only(top: 10.0),
                  scrollDirection: Axis.vertical,
                  children: <Widget>[
                    new Container(
                      padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                      child: Text(
                        this.itemName,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    new Container(
                      padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                      child: Text(
                        this.itemDescription,
                        style: TextStyle(
                          color: Colors.grey[500],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          new Positioned(
            right: 0.5,
            bottom: .0,
            child: new Container(
              width: MediaQuery.of(build).size.width,
              height: 60.0,
              child: Card(
                color: new Color(0xFFF9F9F9),
                child: new Row(
                  children: <Widget>[
                    new Container(
                      margin: new EdgeInsets.symmetric(horizontal: 20.0),
                      child: new DropdownButton<int>(
                        onChanged: (int) {},
                        hint: new Text("Qty: 1"),
                        isDense: true,
                        items: new List<DropdownMenuItem<int>>.generate(
                          10,
                          (int index) => new DropdownMenuItem<int>(
                                value: index,
                                child: new Text(index.toString()),
                              ),
                        ),
                      ),
                    ),
                    
                    new RaisedButton(
                      padding: new EdgeInsets.symmetric(horizontal: 80.0),
                      onPressed: (){},
                      color: Colors.green,
                      child: new Text("Buy for \$15"),
                      textColor: new Color(0xFFF9F9F9),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
    return container;
  }
}
