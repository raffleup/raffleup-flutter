import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext build) {
    Container container = new Container(
        padding: const EdgeInsets.symmetric(vertical: 25.0),
        child: new Column(
          children: <Widget>[
            new Center(
              child: new FractionallySizedBox(
                  widthFactor: 0.5625,
                  child: new Image(image: new AssetImage("assets/logo.png"))),
            ),
          ],
        )
    );
    return container;
  }
}