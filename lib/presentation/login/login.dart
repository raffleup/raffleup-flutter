import 'package:flutter/material.dart';
import 'package:raffle_up/presentation/login/form.dart';
import 'package:raffle_up/presentation/login/logo.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.green[900],
        resizeToAvoidBottomPadding: true,
        body: new Container(
            padding: const EdgeInsets.all(15.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                //1. Logo/Text
                new Logo(),
                //2. Username/Password Form

                new LoginForm(),
                //3. Alternative sign-in options (Google, Facebook, etc)
                //4. Sign up option
              ],
            )
        )
    );
  }
}