import 'package:flutter/material.dart';

class LoginForm extends StatelessWidget {
  @override
  Widget build(BuildContext build) {
    Container container = new Container(
        child: new Form(
            child: new Column(
                children: <Widget>[
                  new Container(
                    child: new TextFormField(
                      //validator: (val) => !val.contains("@") ? "Please enter a valid email.": null,
                      autofocus: false,
                      decoration: new InputDecoration(
                          hintText: "Email",
                          border: InputBorder.none,
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: new Padding(
                              padding: const EdgeInsetsDirectional.only(end: 7.5),
                              child: new Icon(Icons.email))),
                    ),
                    padding: const EdgeInsets.only(bottom: 10.0),
                  ),
                  new Container(
                    child: new TextFormField(
                      obscureText: true,
                      autofocus: false,
                      decoration: new InputDecoration(
                          hintText: "Password",
                          border: InputBorder.none,
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: new Padding(
                              padding: const EdgeInsetsDirectional.only(end: 10.0),
                              child: new Icon(Icons.lock_outline))),
                    ),
                    padding: const EdgeInsets.only(bottom: 10.0),
                  ),
                  new Container(
                    child: new RaisedButton(
                      child: const Text('Login'),
                      onPressed: (){
                        Navigator.pushNamed(build, '/home');;
                      },
                      textColor: Colors.white,
                      color: new Color(0xFF49B136), // Color Of Logo
                    ),
                    padding: const EdgeInsets.only(bottom: 0.0),
                  )
                ]
            )
        )
    );
    return container;
  }
}
